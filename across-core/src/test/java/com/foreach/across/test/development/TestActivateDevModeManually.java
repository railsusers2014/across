/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.test.development;

import com.foreach.across.config.AcrossContextConfigurer;
import com.foreach.across.config.EnableAcrossContext;
import com.foreach.across.core.AcrossContext;
import com.foreach.across.core.EmptyAcrossModule;
import com.foreach.across.core.context.registry.AcrossContextBeanRegistry;
import com.foreach.across.core.development.AcrossDevelopmentMode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Arne Vandamme
 */
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
@ContextConfiguration(classes = TestActivateDevModeManually.Config.class)
@TestPropertySource(properties = "across.development.active=false")
public class TestActivateDevModeManually
{
	@Autowired
	private AcrossContextBeanRegistry beanRegistry;

	@Test
	public void developmentModeShouldBeActive() {
		assertTrue( beanRegistry.getBeanOfType( AcrossDevelopmentMode.class ).isActive() );
	}

	@Test
	public void devModeOnlyBeanShouldBePresent() {
		assertTrue( beanRegistry.containsBean( "devModeOnlyBean" ) );
	}

	@Configuration
	@EnableAcrossContext
	static class Config implements AcrossContextConfigurer
	{
		@Override
		public void configure( AcrossContext context ) {
			context.addModule( new EmptyAcrossModule( "devMode", DevModeOnlyBeanConfiguration.class ) );
			assertFalse( context.isDevelopmentMode() );
			context.setDevelopmentMode( true );
		}
	}
}
