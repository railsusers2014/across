[[development-mode]]
== Development mode
The AcrossContext can have development mode enabled through the *developmentMode* property.
Modules can use development mode to configure different services (or services differently).
An example is auto-reloading and no caching of the message sources if development mode is active.

Apart from setting the *developmentMode* property on `AcrossContext`, development mode can be activated the following way:

. property *across.development.active* is _true_
. Spring profile *dev* is active

.Resource resolving
By default resources (eg. messages, templates) are resolved from the classpath.
It is possible to configure a physical location for the resources of a module if development mode is active (eg on your local filesystem).

_Default location_ +
For simple Across based applications using `@AcrossApplication` it might be enough to simply activate development mode.
If the working directory of the running application contains a *src/main/resources* directory, this directory would be used for all dynamic modules, unless a specific value is set.
This would usually be the case for single maven module projects.

_Specific module resource locations_ +
The path can be configured by adding the right property to the application properties, or specifying them in the development mode properties.
The development mode properties is a special properties file that by default will be looked for in *${user.home}/dev-configs/across-devel.properties*.

A resource location for the development mode properties can be specified by setting the *across.development.properties* property value.
Development mode properties are loaded using resource resolving, so classpath resources can be used as well (eg. _classpath:/dev.properties_).

The development mode properties file should contain properties where the key references the module name, and the value the physical resources directory.

.Example development properties
[source,text,indent=0]
[subs="verbatim,quotes,attributes"]
----
# Absolute directory
acrossModule.MyModule.resources=c:/code/mymodule/src/main/resources
acrossModule.OtherModule.resources=c:/code/othermodule/src/main/resources

# Relative to the working directory
acrossModule.SomeModule.resources=some-module/src/main/resources
----

NOTE: Using relative paths can be an effective way to embed development properties in a multi-module maven project.
Paths should be relative to the working directory of the running application.
Depending on how you run the application, the working directory can differ.

WARNING: Properties added directly to the application properties will take precedence over those present in the development mode properties.
