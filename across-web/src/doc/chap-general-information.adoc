== General information

=== About
{module-name} activates Spring MVC support for all modules in an AcrossContext.
This means modules can configure the Spring MVC support, add controllers, servlets and filters.

On top of that, the out-of-the-box Spring MVC functionality is extended in several ways:

 * a <<thymeleaf-dialect,Thymeleaf dialect>> is provided for interacting with the Across web infrastructure
 * {module-name} provides infrastructure for <<web-templates,templates>>, <<menu-building,menu building>> and <<WebResourceRegistry,registering web resources>>
 * adding support for <<extended-request-mapping,prefixing request mappings and adding custom request condition>>
 * auto configuration of a <<multipart-config,Multipart filter>>
 * auto configuration of static resources with <<client-side-caching,client-side caching>> and <<resource-url-versioning,url versioning>>

{module-name} also contains `<<across-application,@AcrossApplication>>`, a replacement for `@SpringBootApplication` when working with an AcrossContext.

=== Artifact
[source,xml,indent=0]
[subs="verbatim,attributes"]
----
	<dependencies>
		<dependency>
			<groupId>com.foreach.across</groupId>
			<artifactId>across-web</artifactId>
			<version>{across-version}</version>
		</dependency>
	</dependencies>
----

[[across-web-module-settings]]
=== Module settings

All properties start with the *acrossWebModule.* prefix.

|===
|Property |Type |Description |Default

|views.thymeleaf.enabled
|`Boolean`
|Should Thymeleaf view support be enabled.
|_true_

|views.jsp.enabled
|`Boolean`
|Should JSP/JSTL view support be enabled.
|_false_

|resources.path
|`String`
|Relative path for serving all static resources.
|/across/resources

|resources.folders
|`String[]`
|Default subfolders of _views_ that should be served as static resources.
|js,css,static

|resources.versioning.enabled
|`Boolean`
|Auto configure versioning of the default resource resolvers.
|_true_

|resources.versioning.version
|`String`
|Fixed version if resource versioning is enabled.  Default will use build number or module version.
|_null_

|resources.caching.enabled
|`Boolean`
|Auto configure client-side caching of static resources.
|_true_

|resources.caching.period
|`Integer`
|Period for client-side resource caching (if enabled).  Defaults to 1 year.
|_60*60*24*365_

|registerGlobalBuilderContext
|`Boolean`
|Should a request-bound `ViewElementBuilderContext` be created.
|_false_

|templates.enabled
|`Boolean`
|Should support for <<web-templates,web templates>> be enabled.
|_true_

|templates.auto-register
|`Boolean`
|If <<web-templates,web templates>> are enabled, should named templates be autodetected.
|_true_

|multipart.auto-configure
|`Boolean`
|Should a multipart resolver be configured automatically.
|_true_

|multipart.settings
|`MultipartConfigElement`
|Multipart upload settings, if missing defaults will be used.
|_null_

|development-views
|`Map<String,String>`
|Map of physical locations for views resources.  Only used if development mode is active.
|_Collections.emptyMap()_

|===