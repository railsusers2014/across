== What's new in this version?

=== 2.1.0.RELEASE
*Across Core*

* bootstrap locking changes
** the bootstrap lock is now taken and released per installer instead of once for the entire bootstrap phase
** lock owner id is prefixed with both the `AcrossContext#displayName` and the hostname of the server
* <<event-handler-ordering,event handlers are now executed in module bootstrap order>>: event handlers from a previous module will have been executed when yours is called
** this removes an old limitation of event handling in Across

*AcrossWebModule*

* `@AcrossApplication` has a `displayName` attribute to configure the context display name
** if empty, the property value of *across.displayName* will be considered instead
* added `LocalizedTextResolver` for <<localized-text,generic localization of text snippets>>
** `ViewElementBuilderContext` also implements text and message code resolving (see {bootstrap-ui-module-url}[BootstrapUiModule] documentation)
* the default Thymeleaf resolvers now support templates with **.html* suffix
** _.html_ extension is now preferred as text editors will automatically recognize the template as html
** _.thtml_ extension is still fully supported but support may be removed in a future major release

=== 2.0.0.RELEASE
Upgrade to *Spring Platform Athens*, new major dependency versions:

* Spring framework 4.3.x
* Spring Boot 1.4.x
* Thymeleaf 3.x

*Across Core*

* `@AcrossEventHandler` has been deprecated, all beans are now automatically scanned for `@Event` methods
* the `AcrossEventPublisher` interface has some MBassador specific methods removed
** note that the backing implementation of the event bus might change in the future, applications should avoid using MBassdor specific features
* development mode will register a default resources directory for dynamic modules
** this will be an existing *src/main/resources* folder relative to the working directory of the running application
* `@ConditionalOnDevelopmentMode` added to detect if development mode is active

*Across Test*

* The spring-boot-test dependency is now part of across-test
* Updated documentation to use the new `@SpringBootTest` annotation

*AcrossWebModule*

* `ServletContextInitializer` beans in modules are now supported directly for customizing the `ServletContext`
** `@ConditionalOnConfigurableServletContext` and `@ConditionalOnNotConfigurableServletContext` added to determine if `ServletContext` can still be customized
** favour the use of `FilterRegistrationBean` or `ServletRegistrationBean` instead of `AcrossWebDynamicServletConfigurer`, the latter is now deprecated and will be removed in a future release
* Spring Boot devtools are now supported - application context restart will be triggered by default if devtools is on the classpath
* AcrossWebModule now ensures that all `HandlerMapping` beans from other modules are exposed
* a path prefix *@static* and *@resource* is registered to <<web-app-path-resolver,link to static resources>>
* <<thymeleaf-dialect,Thymeleaf dialect>> extensions
** using <<web-app-path-resolver,prefixes>> directly in url expressions is now supported
** addition of *across:static* and *across:resource* attributes for generating links to static resources
* add infrastructure for defining <<customrequestcondition,custom request mapping conditions>> using `@CustomRequestMapping` and `CustomRequestCondition`
* partial rendering of a single `ViewElement` is now also supported (see {bootstrap-ui-module-url}[BootstrapUiModule] documentation).

=== 1.1.3.RELEASE
Maintenance release containing several bugfixes.

=== 1.1.2.RELEASE
*Across Core*

Across now requires JDK8 and has improved support for Spring Boot.
The latter is also a required dependency as of 1.1.2.
A lot of improvements have been done to reduce the amount of boilerplate code necessary to create a new `AcrossContext`.

* <<enableacrosscontext>> annotation now supports autoconfiguration of an `AcrossContext`
* <<moduleconfiguration>> annotation provides a no-hassle way to declare annotated classes that need to be added to a specific module
* message sources are now <<auto-detecting-message-sources,auto-detected>> if they follow the conventions
* both `AcrossModule` and `ModuleBootstrapConfig` now have shortcut `expose()` methods that make it easier to expose additional beans
* several changes and improvements were made to installers:
** installers are now created in their own configurable <<installer-applicationcontext,installer `ApplicationContext`>>
** installers are detected automatically through classpath scanning (defaults to *installers* package of a module)
** installers are ordered based on the presence of an `@Order` annotation
** installers now support `@Conditional` annotations for building more complex conditions
** installers should only be specified as class names in `getInstallers()`, the use of instances is deprecated
** `InstallerSettings` has been refactored to use `InstallerMetaData` instead (breaking change)
** installers can implement `InstallerActionResolver` directly to suppress execution at runtime
** `AcrossInstallerRepository` now has methods to rename installers
** `AcrossLiquibaseInstaller` detects the most appropriate `SchemaConfiguration` to use and modifies default schema accordingly
* development mode can be set through the property *across.development.active* and is active by default if a Spring profile called *dev* is active

*AcrossWebModule*

* <<across-application,@AcrossApplication>> can be used with `SpringApplication` to bootstrap an Across context with dynamic modules and support for embedded servlet containers
* async configuration can now correctly be set through `WebMvcConfigurer#configureAsyncSupport(AsyncSupportConfigurer)`
* `AcrossWebModuleSettings` has been refactored and `AcrossWebModule` can now be <<across-web-module-settings,configured through properties>>
** properties support Spring configuration metadata with possible IDE support
* by default only Thymeleaf views support is activated (breaking change)
* static resources now configure default <<client-side-caching,client-side caching>> and <<resource-url-versioning,resource url versioning>>
* <<dynamic-servlet-registration,dynamic registration of servlets and filters>> now supports ordering
* <<default-http-encoding,default HTTP encoding>> is now forced to UTF-8

*Across Test*

Several improvements have been done for easier integration testing of modules in a web context.

* <<test-builders,test context builders>> have been added for easy configuration of an `AcrossContext` in test methods
* <<mock-across-servlet-context,MockAcrossServletContext>> can now be used for testing of dynamic `ServletContext` configuration
* addition of a `AcrossMockMvcBuilders` class for creating a `MockMvcBuilder` based on an `AcrossContext`
** both <<test-annotations,annotations>> and <<test-builders,builders>> now provide a singleton `MockMvc` instance that is initialized with the bootstrapped context and all dynamically registered filters

=== 1.1.1.RELEASE
Initial public release available on http://search.maven.org/[Maven central].
