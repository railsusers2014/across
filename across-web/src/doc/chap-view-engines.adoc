== TODO: JSP and Thymeleaf integration
If both JSP and Thymeleaf support are enabled, you can easily use both view types at the same time.  The `{module-name}`
 also provides a tag that can be used to import Thymeleaf templates or fragments in a JSP rendering pipeline.  The same
 model (request attributes) should be available in the Thymeleaf template as in the calling JSP.

[source,html,indent=0]
[subs="verbatim,attributes"]
----
<%@ taglib prefix="across" uri="http://across.foreach.com/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>JSP including a Thymeleaf template</title>
</head>
<body>
	<across:thymeleaf template="th/mymodule/thymeleaf-from-jsp-include" />
	<div class="child">
		<across:thymeleaf template="th/mymodule/thymeleaf-from-jsp-include :: fragment" />
	</div>
</body>
</html>
----


mention serving of thymeleaf files